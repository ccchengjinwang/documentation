
<p style="text-align:center;"><img src="img/agl.png" alt="AGL" width="300"></p>

### Welcome to the Automotive Grade Linux (AGL) documentation.

This page will provide a brief overview of the AGL Distribution
and an introduction to selected topics that can help you get a quick
start using AGL for development.

What is Automotive Grade Linux?
-------------------------------

Automotive Grade Linux is a collaborative, open source project
that brings together automakers, suppliers, and technology companies
for the purpose of building Linux-based, open source software platforms
for automotive applications that can serve as de facto industry
standards.

AGL address all software in the vehicle: infotainment,
instrument cluster, heads-up-display (HUD), telematics, connected car,
advanced driver assistance systems (ADAS), functional
safety, and autonomous driving.

Adopting a shared platform across the industry reduces fragmentation
and allows automakers and suppliers to reuse the same code base, which
leads to rapid innovation and faster time-to-market for new products.

AGL is a Linux Foundation project and its goals are as follows:

* Build a single platform for the entire industry
* Develop 70 to 80% of the starting point for a production project
* Reduce fragmentation by combining the best of open source
* Develop an ecosystem of developers, suppliers, and expertise
  that all use a single platform

You can find additional overview information on the
"[About Automotive Grade Linux](https://www.automotivelinux.org/about)" page.
You can find information on the AGL Unified Code Base on the
"[Unified Code Base](https://www.automotivelinux.org/software/unified-code-base)"
page.


What Can I Do Right Away Using AGL?
-----------------------------------

The "Getting Started" topics allow you to quickly accomplish some work using
AGL.
You can use the "Getting Started" sections to do the following:

* [Quickstart](./0_Getting_Started/1_Quickstart/Using_Ready_Made_Images.md) to quickly install the     pre-built images into an emulation or hardware platform.

* [Learn How to Build an AGL Image](./0_Getting_Started/2_Building_AGL_Image/0_Build_Process.md) by working
  through fundamental steps that show you how to build for various supported
  hardware targets (e.g. Raspberry PI boards).

* [Learn How to Create an Application](./3_Developer_Guides/1_Setting_Up_AGL_SDK.md) using the
  application development workflow.


Participate
-----------

The AGL community is diverse and supportive. Anyone can join the mailing list, participate in Zoom meetings, or contribute to any of our projects at any time. You can become an active community member that contributes feedback, ideas, suggestions, bugs and documentation.

**Communications**

AGL uses [groups.io](https://lists.automotivelinux.org/g/agl-main) for communication. The AGL technical community uses the AGL Developer Community mail list. The [agl-dev-community on groups.io contains](https://lists.automotivelinux.org/g/agl-dev-community) a vast [archive](https://lists.automotivelinux.org/g/agl-dev-community/topics) of publicly viewable messages and content. Please [subscribe](https://lists.automotivelinux.org/g/agl-dev-community) to the mail for developer questions, general issues, etc. This is where most of the AGL developer discussions occur. If you need help with something, do not hesitate to post your questions to this list, the AGL Community will help you!

**IRC (Internet Relay Chat)**

Please join us on IRC at the #automotive channel on Libera.chat

If you have not used IRC before, Internet Relay Chat (IRC) is a convenient way to connect to teams and share text-based information in realtime. IRC is widely used by engineers when collaborating on open source projects.

You can access IRC via the web or a traditional client - here is a [list of clients](http://www.irchelp.org/irchelp/clients/) and a primer on [how to use IRC](http://www.irchelp.org/irchelp/new2irc.html) if you are new to it.

If you are behind a firewall, a web client may be best; [irccloud](https://www.irccloud.com) is a popular hosted solution. If you want to host your own service for web access, [shout-irc](http://shout-irc.com) may be of interest and is fully open source. Codethink has setup a live instance of shout-irc at [chat.codethink.co.uk](https://chat.codethink.co.uk) for AGL community members to try out.

**Recurring Meetings**

We have a weekly developer meeting and a number of other recurring Expert Group meetings that everyone is invited to participate in. The complete list of meetings and their information is available on AGL [Google Calendar](https://lists.automotivelinux.org/g/agl-dev-community/calendar). You can subscribe to AGL [Google Calendar](https://lists.automotivelinux.org/g/agl-dev-community/calendar) by logging to your LF account and clicking subscribe to calendar button at the bottom of AGL [Google Calendar](https://lists.automotivelinux.org/g/agl-dev-community/calendar) page. 
